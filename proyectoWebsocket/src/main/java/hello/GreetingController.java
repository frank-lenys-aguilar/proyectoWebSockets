package hello;

import hello.entity.Gasolina;
import hello.service.GasolinaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

import java.util.List;



@Controller
@Slf4j
public class GreetingController {

    static boolean flag=false;

    @Autowired
    GasolinaService gasolinaService;


    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greeting(HelloMessage message) throws Exception {

        String val="";

        //if(message.getName()==-1 && flag==false){
            List<Gasolina> gal= gasolinaService.listar();
            for(Gasolina f:gal){
                val+=f.getFecha()+",";
                val+=f.getHora()+",";
                val+=f.getPlaca()+",";
                val+=f.getCantidad()+",";
                val+=f.getCosto()+",";
                val+=f.getIidd()+",";
                val+=f.getUuid()+",";
                val+=f.getPos()+",";
                val+="|";
                flag=true;
            }
       /* }else {
            if(message.getName()!=gasolinaService.listar().size()-1){
                Gasolina f= gasolinaService.buscar();
                val+=f.getFecha()+",";
                val+=f.getHora()+",";
                val+=f.getPlaca()+",";
                val+=f.getCantidad()+",";
                val+=f.getCosto()+",";
                val+=f.getIidd()+",";
                val+=f.getUuid()+",";
                val+=f.getPos()+",";
                val+="|";
            }
        }*/
        log.info("Valores : "+val);
        return new Greeting(HtmlUtils.htmlEscape(val));
    }

}
