package hello.service;

import hello.entity.Gasolina;
import hello.model.GasolinaModel;
import hello.repository.GasolinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GasolinaService {

    @Autowired
    private GasolinaRepository gasolinaRepository;

    public List<Gasolina> listar(){
        return gasolinaRepository.findAll();
    }

    public Gasolina buscar(){
        return gasolinaRepository.findByPos(gasolinaRepository.findAll().size()-1);
    }
}
