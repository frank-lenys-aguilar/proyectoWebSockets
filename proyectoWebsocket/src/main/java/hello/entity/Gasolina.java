package hello.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Gasolina {

    String fecha;
    String hora;
    String placa;
    int cantidad;
    int costo;
    String iidd;
    String uuid;
    String estado;
    int pos;
}
