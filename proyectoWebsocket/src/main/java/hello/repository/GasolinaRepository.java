package hello.repository;

import hello.entity.Gasolina;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
@Repository
public interface GasolinaRepository extends MongoRepository<Gasolina,Integer> {

    @Query("{ pos : ?0 }")
    Gasolina findByPos(int pos);
}
