package hello.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GasolinaModel {
    String fecha;
    String hora;
    String placa;
    int cantidad;
    int costo;
    String id;
    String transaccion;
}
